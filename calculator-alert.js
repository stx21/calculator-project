var angka1 = parseInt(prompt('Masukkan angka pertama'));
var angka2 = parseInt(prompt('Masukkan angka kedua'));
var operator = prompt('pilih operator (+ - / * ^ max min)');

if (operator == '+') {
  alert(angka1 + angka2);
} else if (operator == '-') {
  alert(angka1 - angka2);
} else if (operator == '/') {
  alert(angka1 / angka2);
} else if (operator == '*') {
  alert(angka1 * angka2);
} else if (operator.toLowerCase() == '^') {
  alert(Math.pow(angka1, angka2));
} else if (operator.toLowerCase() == 'max') {
  alert(Math.max(angka1, angka2));
} else if (operator.toLowerCase() == 'min') {
  alert(Math.min(angka1, angka2));
} else {
  alert('Masukkan operator yg sesuai atau angka lain');
}

// isNaN dipakai untuk validasi >> https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN

// export const x = 2;
// export function multiply(a, b) {
//   return a * b;
// }
// export function multiplyWithx(y) {
//   return x * y;
// }
